import UIKit
import Flutter
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?)
        -> Bool {
            FirebaseApp.configure()	
            return true

    }

    //  override func application(
    //     GeneratedPluginRegistrant.register(with: self)
    //     channel = FlutterMethodChannel.init(name: "com.acryliccode.franetestimonies",binaryMessenger: controller);
}
