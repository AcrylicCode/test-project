import 'package:flutter/material.dart';
import 'home.dart';
import 'src/widgets/addEntry.dart';
import 'src/widgets/welcomePage.dart';

void main() => runApp(MaterialApp(
      home: Home(),
      routes: <String, WidgetBuilder>{
        'home': (BuildContext context) => new Home(),
        'welcome': (BuildContext context) => new WelcomePage(),
        '/addEntry': (BuildContext context) => new AddEntry(),
      },
    ));
