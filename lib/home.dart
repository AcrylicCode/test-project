import 'package:flutter/material.dart';
import 'src/init.dart' as appInit;
import 'src/widgets/listEntries.dart';
import 'src/firebase/auth.dart' as fbAuth;
import 'src/firebase/database.dart' as fbDatabase;
import 'package:firebase_database/firebase_database.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:connectivity/connectivity.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextEditingController usernameController = TextEditingController();
  SharedPreferences prefs;
  bool isConnected = true;
  String titleStatus = '';

  @override
  initState() {
    super.initState();
    _doInit();
  }

  Future<Null> _doInit() async {
     await appInit.init();
    _getUsername();
    initConnectivity();
  }

  void initConnectivity() async {
    ConnectivityResult connection = await Connectivity().checkConnectivity();
    if (connection == ConnectivityResult.none) {
      setState(() {
        isConnected = false;
        titleStatus = 'Offline';
      });
    } else {
      fbDatabase.uploadOfflineEntries();
    }

    Connectivity()
        .onConnectivityChanged
        .listen(_onConnectivityChanged, cancelOnError: true);
  }

  void _onConnectivityChanged(ConnectivityResult result) {
    print('Connection is: ${result.toString()}');
    fbDatabase.uploadOfflineEntries();
    setState(() {
      if (result == ConnectivityResult.none) {
        isConnected = false;
        titleStatus = '- Offline';
      } else {
        isConnected = true;
        titleStatus = '';
      }
    });
  }

  Future<Null> _getUsername() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      try {
        usernameController.text = prefs.getString('username');
        bool correctPassword = prefs.getBool('correctPassword');
        if (usernameController.text.isEmpty && correctPassword) {
          Navigator.of(context).pushNamed('welcome');
          return;
        }
        print('Got user name as: ${usernameController.text}');
      } catch (e) {
        //show screen to add user details
        Navigator.of(context).pushNamed('welcome');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text(
          'Testimonies $titleStatus',
          style: TextStyle(fontSize: 30.0),
        ),
      ),
      drawer: new Drawer(
        child: new Container(
          padding: EdgeInsets.fromLTRB(15.0, 50, 15.0, 15.0),
          child: new Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Text('Update Your Info', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30.0),),
              Container(height: 10.0,),
              TextField(
                controller: usernameController,
                textCapitalization: TextCapitalization.words,
                decoration: InputDecoration(labelText: 'What is your name?'),
              ),
              Container(height: 5.0,),
              new RaisedButton(
                child: new Text('Update Name'),
                onPressed: () {
                  prefs.setString('username', usernameController.text);
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: new FloatingActionButton(
        onPressed: () => Navigator.of(context).pushNamed('/addEntry'),
        child: Icon(Icons.add),
      ),
      body: new Container(
        padding: new EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
        child: new ListEntries(isConnected),
      ),
    );
  }
}
