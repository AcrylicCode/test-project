import 'package:location/location.dart';
import 'package:geocoder/geocoder.dart';
import 'package:firebase_database/firebase_database.dart';

class Entry {
  String entryName = ''; //filename
  String name = '';
  DateTime date;
  double latitude = 0.0;
  double longitude = 0.0;
  String locationName = '';
  String enteredBy = '';
  String contactInfo = '';
  String category = '';
  String body = '';

  //default constructor
  Entry(this.entryName, this.name, this.date, this.latitude, this.longitude,
      this.enteredBy, this.contactInfo, this.category, this.body);

  Entry.fromSnapshot(DataSnapshot snapshot) {
    if (snapshot.value['entryName'] != null)
      this.name = snapshot.value['entryName'];
    if (snapshot.value['name'] != null) this.name = snapshot.value['name'];
    if (snapshot.value['date'] != null)
      this.date = DateTime.parse(snapshot.value['date']);
    if (snapshot.value['location'] != null)
      this.locationName = snapshot.value['location'];
    if (snapshot.value['enteredBy'] != null)
      this.enteredBy = snapshot.value['enteredBy'];
    if (snapshot.value['contactInfo'] != null)
      this.contactInfo = snapshot.value['contactInfo'];
    if (snapshot.value['category'] != null)
      this.category = snapshot.value['category'];
    if (snapshot.value['body'] != null) this.body = snapshot.value['body'];
  }

  //encode data and return JSON
  Map<String, dynamic> toJson() {
    return {
      'entryName': entryName,
      'name': name,
      'date': date.toString(),
      'latitude': latitude,
      'longitude': longitude,
      'enteredBy': enteredBy,
      'contactInfo': contactInfo,
      'category': category,
      'body': body,
    };
  }

  //decode JSON back to Offline Entry Class
  Entry.fromJson(Map json) {
    if (json['entryName'] != null) this.entryName = json['entryName'];
    if (json['name'] != null) this.name = json['name'];
    if (json['date'] != null) this.date = DateTime.parse(json['date']);
    if (json['latitude'] != null && json['longitude'] != null) {
      this.latitude = json['latitude'] as double;
      this.longitude = json['longitude'] as double;
    }
    if (json['enteredBy'] != null) this.enteredBy = json['enteredBy'];
    if (json['contactInfo'] != null) this.contactInfo = json['contactInfo'];
    if (json['category'] != null) this.category = json['category'];
    if (json['body'] != null) this.body = json['body'];
  }
}
