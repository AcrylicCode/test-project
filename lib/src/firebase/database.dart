import 'dart:io';
import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'entry.dart';

import 'package:location/location.dart';
import 'package:geocoder/geocoder.dart';

FirebaseApp app;
FirebaseDatabase database;

Future<Null> init(FirebaseDatabase database) async {
  // try {
  //   if (Platform.isAndroid) {
      
  //   }
  //   if (Platform.isIOS) {
  //     app = await FirebaseApp.configure(
  //       name: 'firebaseapp',
  //       options: new FirebaseOptions(
  //         googleAppID: '1:1055983274686:ios:26c90f321fa374f9',
  //         gcmSenderID: "1055983274686",
  //         apiKey: 'AIzaSyDyRgQPzJmwSygekDcpdRn8jz8VvcLEZkU',
  //         projectID: 'france-testimonies',
  //         databaseURL: 'https://france-testimonies.firebaseio.com',
  //       ),
  //     );
  //   }
  // } catch (Error) {
  //   print(Error);
  // }

  
  return;
}

Future<Null> addTestimony(String name, String category, String body,
    String enteredBy, String contactInfo, bool isConnected) async {
  DateTime _date = DateTime.now();
  String _entryName = '${_date.year}${_date.month}${_date.day}-$name-$category';

  if (isConnected) {
    //get location info from device
    String _location = await getLocationNamed();

    //save actual database reference
    DatabaseReference _testimonyRef;
    _testimonyRef =
        FirebaseDatabase.instance.reference().child('testimonies/$_entryName');
    _testimonyRef.update(<String, String>{'name': name});
    _testimonyRef.update(<String, String>{'date': '$_date'});
    _testimonyRef.update(<String, String>{'location': '$_location'});
    _testimonyRef.update(<String, String>{'enteredBy': enteredBy});
    _testimonyRef.update(<String, String>{'contactInfo': contactInfo});
    _testimonyRef.update(<String, String>{'category': category});
    _testimonyRef.update(<String, String>{'body': body});
  } else {
    //get location info from device
    Location location = new Location();
    LocationData currentLocation = await location.getLocation();

    Entry entry = new Entry(_entryName, name, _date, currentLocation.latitude,
        currentLocation.longitude, enteredBy, contactInfo, category, body);
    _saveOfflineEntry(entry);
  }
  return null;
}

Future<Null> _saveOfflineEntry(Entry entry) async {
  File file = await File(
          '${Directory.systemTemp.path}/offlineEntries/${entry.entryName}.json')
      .create(recursive: true);
  print(json.encode(entry.toJson()));
  file.writeAsString(json.encode(entry.toJson()));
}

List<Entry> getOfflineEntries() {
  Directory directory =
      Directory('${Directory.systemTemp.path}/offlineEntries');
  List<FileSystemEntity> files;
  files = directory.listSync(recursive: true, followLinks: false);
  List<Entry> entries = new List<Entry>();

  for (int i = 0; i < files.length; i++) {
    File file = files[i] as File;
    try {
      Entry entry = Entry.fromJson(json.decode(file.readAsStringSync()));
      entries.add(entry);
    } catch (Exception) {}
  }

  return entries;
}

Future<Null> uploadOfflineEntries() async {
  List<Entry> entries = getOfflineEntries();
  if (entries.length == 0) return;

  for (int i = 0; i < entries.length; i++) {
    Entry entry = entries[i];
    String location =
        await getLocationFromCoordinates(entry.latitude, entry.longitude);

    DatabaseReference _ref = FirebaseDatabase.instance
        .reference()
        .child('testimonies/${entry.entryName}');
    _ref.update(<String, String>{'name': entry.name});
    _ref.update(<String, String>{'date': '${entry.date.toString()}'});
    _ref.update(<String, String>{'location': '$location'});
    _ref.update(<String, String>{'enteredBy': entry.enteredBy});
    _ref.update(<String, String>{'contactInfo': entry.contactInfo});
    _ref.update(<String, String>{'category': entry.category});
    _ref.update(<String, String>{'body': entry.body});

    String path =
        '${Directory.systemTemp.path}/offlineEntries/${entry.entryName}.json';
    if (File(path).existsSync()) {
      try {
        File(path).deleteSync(recursive: false);
      } catch (e) {}
    }

    print('Uploaded ${entry.entryName}');
  }
}

Future<String> getLocationNamed() async {
  Location location = new Location();
  LocationData currentLocation = await location.getLocation();
  var _addresses = await Geocoder.local
      .findAddressesFromCoordinates(
          new Coordinates(currentLocation.latitude, currentLocation.longitude))
      .catchError((error) {
    return '';
  });
  return '${_addresses.first.locality}, ${_addresses.first.adminArea}, ${_addresses.first.countryName}';
}

Future<String> getLocationFromCoordinates(
    double latitude, double longitude) async {
  var _addresses = await Geocoder.local
      .findAddressesFromCoordinates(new Coordinates(latitude, longitude))
      .catchError((error) {});
  if (_addresses != null && _addresses.length != 0)
    return '${_addresses.first.locality}, ${_addresses.first.adminArea}, ${_addresses.first.countryName}';
  else
    return '';
}
