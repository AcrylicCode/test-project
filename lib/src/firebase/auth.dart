import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;

Future<bool> signOut() async {
  await _auth.signOut();
  return true;
} 

Future<Null> ensureLoggedIn() async {
  FirebaseUser firebaseUser = await _auth.currentUser();
  assert (firebaseUser != null);
}

Future<bool> signIn() async {
  print('Started sign in');
  FirebaseUser user = await _auth.signInAnonymously();
  print('Finished sign in');
  print(_auth.currentUser().toString());

  if(user != null){
    return true;
  } else {
    return false;
  }
}

Future<String> username() async {
  await ensureLoggedIn();
  FirebaseUser firebaseUser = await _auth.currentUser();
  return firebaseUser.uid;
}