import 'dart:async';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';

FirebaseApp app;
FirebaseDatabase database;

Future<Null> init() async {
  app = await FirebaseApp.configure(
    name: 'firebaseapp',
    options: new FirebaseOptions(
      googleAppID: '1:1055983274686:android:26c90f321fa374f9',
      gcmSenderID: "1055983274686",
      apiKey: 'AIzaSyAiAUoZWqkBjH72h4A1fmahFqxpWM_P2i4',
      projectID: 'france-testimonies',
      databaseURL: 'https://france-testimonies.firebaseio.com',
    ),);

  database = new FirebaseDatabase(app: app);
}