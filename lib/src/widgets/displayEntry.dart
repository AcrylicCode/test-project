import 'package:flutter/material.dart';
import '../firebase/entry.dart';

class DisplayEntry extends StatefulWidget {
  DisplayEntry(this.entry);
  Entry entry;
  @override
  _DisplayEntryState createState() => _DisplayEntryState(entry);
}

class _DisplayEntryState extends State<DisplayEntry> {
  _DisplayEntryState(this.entry);
  Entry entry;
  bool isExpanded = false;

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: () {
        setState(() {
          isExpanded = !isExpanded;
        });
      },
      child: new Container(
        padding: EdgeInsets.only(top: 10.0),
        child: new Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            _displayName(entry),
            _displayContactInfo(entry),
            _displayDate(entry),
            _displayEnteredBy(entry),
            _displayLocation(entry),
            new Container(height: 5.0),
            _displayBody(entry),
            new Container(
              height: 2.0,
              color: Colors.black,
              margin: EdgeInsets.only(top: 10.0),
            ),
          ],
        ),
      ),
    );
  }

  Widget _emptyContainer() {
    return new Container(
      height: 0.0,
    );
  }

  Widget _displayName(Entry entry) {
    if (entry.name != '') {
      return new Text(
        '${entry.category} - ${entry.name}',
        style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
      );
    } else {
      return _emptyContainer();
    }
  }

  Widget _displayDate(Entry entry) {
    if (entry.date != null) {
      DateTime _date = entry.date;
      return new Text(
        '${_date.month}-${_date.day}-${_date.year}',
        style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.normal),
      );
    } else {
      return _emptyContainer();
    }
  }

  Widget _displayEnteredBy(Entry entry) {
    if (isExpanded && entry.enteredBy != '') {
      return new Text(
        'Added By: ${entry.enteredBy}',
        style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.normal),
      );
    } else {
      return _emptyContainer();
    }
  }

  Widget _displayLocation(Entry entry) {
    if (entry.locationName != '') {
      return new Text(
        '${entry.locationName}',
        style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.normal),
      );
    } else {
      return _emptyContainer();
    }
  }

  Widget _displayContactInfo(Entry entry) {
    if (isExpanded && entry.contactInfo != '') {
      return new Text(
        'Contact: ${entry.contactInfo}',
        style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.normal),
      );
    } else {
      return _emptyContainer();
    }
  }

  Widget _displayBody(Entry entry) {
    if (entry.body != '') {
      String _body = entry.body;
      if (!isExpanded && _body.length > 100) {
        _body = _body.substring(0, 100);
      }

      return new Text(
        _body,
        style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.normal),
      );
    } else {
      return _emptyContainer();
    }
  }
}