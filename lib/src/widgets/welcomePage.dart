import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class WelcomePage extends StatefulWidget {
  @override
  WelcomePageState createState() => WelcomePageState();
}

class WelcomePageState extends State<WelcomePage> {
  SharedPreferences prefs;
  TextEditingController nameController = TextEditingController();
  String nameStatus = '';
  TextEditingController passwordController = TextEditingController();
  String passwordStatus = '';

  @override
  initState() {
    super.initState();
    _getPrefs();
  }

  Future<Null> _getPrefs() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: new Container(
          padding: EdgeInsets.all(30.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Text(
                'Welcome!',
                style:
                    new TextStyle(fontSize: 40.0, fontWeight: FontWeight.bold),
              ),
              new TextField(
                controller: nameController,
                textCapitalization: TextCapitalization.words,
                decoration:
                    new InputDecoration(labelText: 'What is your name?'),
              ),
              new Text(
                nameStatus,
                style: new TextStyle(color: Colors.red, fontSize: 10.0),
              ),
              new Container(
                height: 15.0,
              ),
              new TextField(
                controller: passwordController,
                textCapitalization: TextCapitalization.words,
                decoration:
                    new InputDecoration(labelText: 'What is the password?'),
              ),
              new Text(
                passwordStatus,
                style: new TextStyle(color: Colors.red, fontSize: 10.0),
              ),
            ],
          ),
        ),
        floatingActionButton: new FloatingActionButton(
          child: Icon(Icons.save),
          onPressed: () {
            if (nameController.text.isNotEmpty &&
                passwordController.text.isNotEmpty &&
                passwordController.text == 'FranceShallBeSaved') {
              prefs.setString('username', nameController.text);
              prefs.setBool('correctPassword', true);
              Navigator.of(context).pushReplacementNamed('home');
              nameStatus = '';
              passwordStatus = '';
            } else {
              setState(() {
                if (nameController.text.isEmpty)
                  nameStatus = 'Must set your name before submitting';
                if (passwordController.text.isEmpty)
                  passwordStatus = 'Must enter the password before submitting';
                else
                  passwordStatus = 'Wrong password';
              });
            }
          },
        ),
      ),
    );
  }
}
