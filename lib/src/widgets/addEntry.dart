import 'package:flutter/material.dart';
import '../firebase/database.dart' as fbDatabase;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:connectivity/connectivity.dart';
import '../firebase/entry.dart';
import 'package:location/location.dart';

class AddEntry extends StatefulWidget {
  @override
  AddEntryState createState() => AddEntryState();
}

class AddEntryState extends State<AddEntry> {
  TextEditingController nameController = TextEditingController();
  String categoryValue = 'Salvation';
  TextEditingController bodyController = TextEditingController();
  TextEditingController contactController = TextEditingController();
  SharedPreferences prefs;

  void _saveEntry() async {
    String _username;
    prefs = await SharedPreferences.getInstance();
    try {
      _username = await prefs.getString('username');
    } catch (e) {
      _username = '';
    }

    bool isConnected = true;
    if (await Connectivity().checkConnectivity() == ConnectivityResult.none) {
      isConnected = false;
    }

    await fbDatabase.addTestimony(nameController.text, categoryValue,
        bodyController.text, _username, contactController.text, isConnected);

    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Add Entry'),
      ),
      floatingActionButton: new FloatingActionButton(
        onPressed: _saveEntry,
        child: Icon(Icons.save),
      ),
      body: Container(
          padding: EdgeInsets.all(30.0),
          child: new ListView(
            children: <Widget>[
              //NAME ENTRY
              new TextField(
                autofocus: true,
                autocorrect: false,
                style: TextStyle(fontSize: 20.0),
                textCapitalization: TextCapitalization.words,
                controller: nameController,
                onChanged: (v) => nameController.text,
                decoration: InputDecoration(
                    labelText: 'Name - Who this testimony is about'),
              ),
              //CONTACT INFO
              new Container(
                height: 15.0,
              ),
              new TextField(
                autocorrect: false,
                style: TextStyle(fontSize: 20.0),
                textCapitalization: TextCapitalization.none,
                controller: contactController,
                onChanged: (v) => contactController.text,
                decoration:
                    InputDecoration(labelText: 'Contact info for follow-up'),
              ),
              //CATEGORY ENTRY
              new Container(
                height: 15.0,
              ),
              new DropdownButton<String>(
                  value: categoryValue,
                  items: <String>[
                    'Salvation',
                    'Healing',
                    'Deliverance',
                    'Dream-Interpretation',
                    'Finances'
                  ].map((String value) {
                    return new DropdownMenuItem<String>(
                      value: value,
                      child: new Text(value, style: TextStyle(fontSize: 20.0)),
                    );
                  }).toList(),
                  onChanged: (String newValue) {
                    setState(() {
                      categoryValue = newValue;
                    });
                  }),
              //BODY ENTRY
              new Container(
                height: 15.0,
              ),
              new TextField(
                autocorrect: true,
                style: TextStyle(fontSize: 20.0),
                textCapitalization: TextCapitalization.sentences,
                controller: bodyController,
                onChanged: (v) => bodyController.text,
                decoration: InputDecoration(
                    labelText: 'Short description of what happened'),
              ),
            ],
          )),
    );
  }
}
