import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:convert';
import 'package:path_provider/path_provider.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import '../firebase/database.dart' as fbDatabase;
import '../firebase/entry.dart';
import 'displayEntry.dart';

class ListEntries extends StatefulWidget {
  ListEntries(this.isConnected);
  bool isConnected = true;
  @override
  _ListEntriesState createState() => _ListEntriesState(isConnected);
}

class _ListEntriesState extends State<ListEntries> {
  _ListEntriesState(this.isConnected) {
    //_getOfflineList();
  }
  bool isConnected = true;

  @override
  Widget build(BuildContext context) {
    // if (isConnected) {
    try {
      return FirebaseAnimatedList(
        query: FirebaseDatabase.instance
            .reference()
            .child("testimonies")
            .orderByChild("date"),
        // reverse: true,
        scrollDirection: Axis.vertical,
        itemBuilder: (BuildContext context, DataSnapshot snapshot,
            Animation<double> animation, int x) {
          // print(snapshot.value);
          return DisplayEntry(Entry.fromSnapshot(snapshot));
        },
      );
    } catch (Exception) {
      return new Container();
    }
  }

//   List<Widget> _getOfflineList() {
//     String directoryPath = '${Directory.systemTemp.path}/offlineEntries';
//     if (Directory(directoryPath).existsSync()) {
//       Directory directory =
//           Directory('${Directory.systemTemp.path}/offlineEntries');
//       List<File> files =
//           directory.listSync(recursive: false, followLinks: false);

//       for (int i = 0; i < files.length; i++) {
//         setState(() {
//           Entry entry = Entry.fromJson(json.decode(files[i].readAsStringSync()));
//         DisplayEntry newWidget = new DisplayEntry(entry);
//         offlineList.add(newWidget);
//         });
//       }
//     }
//     else {
//       offlineList.add(new Container());
//     }
//     return offlineList;
//   }
}
