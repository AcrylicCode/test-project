import 'dart:async';
import 'dart:io';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'firebase/auth.dart' as fbAuth;
import 'firebase/database.dart' as fbDatabase;
//import 'firebase/firebaseConnections.dart' as fbConnect;

Future<Null> init() async {
  await _createOfflineFolder();
  await _initApp();
  await fbAuth.signIn();

  //await fbDatabase.init(await _initApp());
}

Future<FirebaseDatabase> _initApp() async {
  FirebaseApp app;
  FirebaseDatabase database;
  app = await FirebaseApp.configure(
    name: 'firebaseapp',
    options: new FirebaseOptions(
      googleAppID: '1:1055983274686:android:26c90f321fa374f9',
      gcmSenderID: "1055983274686",
      apiKey: 'AIzaSyAiAUoZWqkBjH72h4A1fmahFqxpWM_P2i4',
      projectID: 'france-testimonies',
      databaseURL: 'https://france-testimonies.firebaseio.com',
    ),
  );
  database = new FirebaseDatabase(app: app);

  database.setPersistenceEnabled(true);
  database.setPersistenceCacheSizeBytes(100000);
  return database;
}

Future<Null> _createOfflineFolder() async {
  String directoryPath = '${Directory.systemTemp.path}/offlineEntries';
  if (await Directory(directoryPath).exists() == false) {
    Directory(directoryPath).create(recursive: true);
  }
  return;
}
